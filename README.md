#tag-lang-cli
前端多语言方案

#使用方式

官网文档： http://nodejs.cn/api/fs

##安装

>npm i -g tag-lang-cli


##命令行：

 Usage: tag-lang-cli -s 源文件夹 -t 目标文件夹 -f 文件类型 -c 配置 -l 当前语言类型标签 -e


  Options:

    -V, --version                   output the version number
    --source <源文件夹>             
    -target [目标文件夹]            
                如果为空，则等于 source。 
                如果 source 是以 _lang结尾，则 target 用语言代码替换结尾的 lang。
    -file-type <文件类型>          如：-f js,css
    -c, --config <配置>               使用伪Json格式，key表示tagName,value表示语言代码。如：-c c:cn,e:en
    -l, --language [当前语言]           仅编译当前语言，配置项的key值。
    -e, --as-service [编译完后，后台继续运行]  
    -h, --help                      output usage information

如：tag-lang-cli --file-type vue,js,html --config c:cn,e:en  -e --source ../pzx_vue2/src_lang  -l c
 

##原理
监控一个文件夹，对文件进行转换，并把转换后的内容保存到另一个文件夹中。 
转换的规则是：

    <div><c>中文资源</c><e>英文资源</e></div>
如以上命令行。
把 中文资源保留，把英文资源去除。以上写法，在任何位置都有效。 
如：标签，字符串，注释。
转换结果显示： 

    中文资源

##简写
简写时转义，编译的内容不能包含： 
* 大于号 >
* 小于号 <
* 双引号 "
* 单引号 '
如果编译的内容没有特殊字符，那么可以简写成：
```html
<div>中文资源<ce>英文资源</div>
```
<ce>前面的部分，先遇到 双引号(")，单引号(') 表示在字符串中，会向后找闭合引号; 先遇到 大于号(>) 表示在Dom中,会向后找 小于号(<)。

以下情况会编译出错，不能使用简写：

```html
<div>"鲁迅说:<ce>‘luxun says:</div>
<div>鲁迅说:><ce>luxun says:></div>
```
    
##对webpack进行改造

    src_lang 保存源码。
    src_cn   保存转换的中文站点
    src_en   保存转换的英文站点
    
    dist_cn  保存打包的中文站点
    dist_en  保存打包的英文站点


